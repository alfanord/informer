FROM python:3.5

WORKDIR /usr/src/app

COPY . .

RUN pip install --no-cache-dir -r requirements.txt && \
    apt-get update && apt-get install --no-install-recommends -y locales sox && \
    locale-gen ru_RU.UTF-8 ru_RU && \
    localedef -i ru_RU -c -f UTF-8 -A /usr/share/locale/locale.alias ru_RU.UTF-8 && \
    localedef -i ru_RU -c -f UTF-8 -A /usr/share/locale/locale.alias ru_RU && \
    apt-get clean all

ENV TZ=Europe/Moscow \
    LANG=ru_RU.UTF-8 \
    LANGUAGE=ru_RU.UTF-8

CMD [ "python", "app.py" ]
