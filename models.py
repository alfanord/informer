# -*- coding: utf-8 -*-
# noinspection SpellCheckingInspection

import config, locale

from datetime import datetime, timedelta

from contextlib import contextmanager
from sqlalchemy import create_engine

from sqlalchemy import Table, Column, DateTime, String, Integer, ForeignKey, PrimaryKeyConstraint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, scoped_session

from sqlalchemy.dialects.postgresql import JSON, UUID

engine = create_engine(config.SQLALCHEMY_DATABASE_URI)
engine.echo = False

Base = declarative_base()

object_persons = Table('object_persons',
                       Base.metadata,
                       Column('object_id', Integer, ForeignKey('objects.id'), nullable=False),
                       Column('person_id', Integer, ForeignKey('persons.id'), nullable=False),
                       PrimaryKeyConstraint('object_id', 'person_id'))


class Importance(Base):
    __tablename__ = 'objectimportance'

    id = Column(Integer, primary_key=True)
    priority = Column(Integer, nullable=False)
    _title = Column('title', String(255))
    _comment = Column('comment', String(255))

    objects = relationship('Object', backref="importance", lazy='dynamic')

    @property
    def title(self):
        return self._title.strip()

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def comment(self):
        return self._comment.strip()

    @comment.setter
    def comment(self, value):
        self._comment = value

    def __repr__(self):
        return '<Importance {}>'.format(self.title)


class Object(Base):
    __tablename__ = 'objects'

    id = Column(Integer, primary_key=True)
    status = Column(Integer)
    title = Column(String(255))
    comm_account = Column(Integer)
    comm_section = Column(Integer)
    media = Column(UUID(as_uuid=True))

    importance_id = Column(Integer, ForeignKey('objectimportance.id'))

    persons = relationship('Person', secondary=object_persons, back_populates='objects')

    partitions = relationship("Partition", back_populates='object')
    messages = relationship("Message", backref="object", lazy='dynamic')

    @property
    def code(self):
        return str(self.comm_account) if self.comm_account is not None else str(self.id)

    @property
    def panel_code(self):
        return '{:04d}'.format(self.comm_account if self.comm_account is not None else self.id)

    def __repr__(self):
        return '<Object {}>'.format(self.title)


class Partition(Base):
    __tablename__ = 'partitions'
    id = Column(Integer, primary_key=True)
    code = Column(Integer)
    title = Column(String(255))
    state = Column(JSON)

    object_id = Column(Integer, ForeignKey('objects.id'))

    object = relationship("Object", back_populates="partitions")

    @property
    def panel_code(self):
        return '{:02d}'.format(self.code)

    def __repr__(self):
        return '<Partition {}, {}>'.format(self.code, self.title)


class Person(Base):
    __tablename__ = 'persons'

    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    surname = Column(String(255))
    middle_name = Column(String(255))
    email = Column(String(255))
    status = Column(String(255))
    keyword = Column(String(255))
    comment = Column(String(255))
    phone_mobile = Column(String(11))
    media = Column(UUID(as_uuid=True))

    objects = relationship("Object", secondary=object_persons, back_populates="persons")

    @property
    def full_name(self):
        return '{} {}'.format(self.name, self.surname)

    @property
    def priority(self):
        if len(self.objects) == 0:
            return 0, ''
        elif self.objects[0].importance is None:
            return 0, ''
        else:
            return self.objects[0].importance.priority, self.objects[0].importance.title

    def __repr__(self):
        return '<Person {}>'.format(self.surname)


class EventType(Base):
    __tablename__ = 'eventtype'

    id = Column(Integer, primary_key=True)
    priority = Column(Integer, nullable=False)
    is_incident = Column('generate_incident', Integer)
    _type_ru = Column('type_ru', String(255))
    _trouble_ru = Column('trouble_ru', String(255))

    messages = relationship("Message", backref="event_type", lazy='dynamic')

    @property
    def type_ru(self):
        return self._type_ru.strip()

    @type_ru.setter
    def type_ru(self, value):
        self._type_ru = value

    @property
    def trouble_ru(self):
        return self._trouble_ru.strip()

    @trouble_ru.setter
    def trouble_ru(self, value):
        self._trouble_ru = value

    def __iter__(self):
        all_events = self.query.all()
        return (x for x in all_events)

    def __repr__(self):
        return '<EventType {}, {}>'.format(self.id, self.trouble_ru)


class Message(Base):
    __tablename__ = 'messages'

    id = Column(Integer, primary_key=True)
    event_code = Column(String(3), nullable=False)
    _qualifier = Column('qualifier', Integer, nullable=False)
    partition_code = Column(String(2), nullable=False)
    zone_code = Column(String(3), nullable=False)

    event_text = Column(String(3000))
    user_comment = Column(String(3000))

    timestamp = Column(DateTime(timezone=True))

    object_id = Column(Integer, ForeignKey('objects.id'))
    event_type_id = Column(Integer, ForeignKey('eventtype.id'))

    @property
    def human_date(self):
        locale.setlocale(locale.LC_TIME, "ru_RU.UTF-8")
        return datetime.strftime(self.timestamp, '%b %d %H:%M:%S')

    @property
    def human_date_comma(self):
        locale.setlocale(locale.LC_TIME, "ru_RU.UTF-8")
        return datetime.strftime(self.timestamp, '%b %d, %H:%M:%S')
        # return datetime.strftime(self.timestamp, '%H:%M:%S')

    @property
    def panel_code(self):
        return '{:04d}'.format(self.object_id if self.object_id is not None else 0)

    @property
    def qualifier(self):
        return 'E' if self._qualifier == 1 else 'R'

    @qualifier.setter
    def qualifier(self, value):
        self._qualifier = 1 if value in ['1', 'E'] else 3

    @property
    def contact_id(self):
        # return '{o:04d}{q}{e:03d}{p:02d}{z:03d}'.format(o=self.object_id, q=self.qualifier, e=self.event_code, p=self.partition_code, z=self.zone_code)
        return '{q}{e}{p}{z}'.format(q=self.qualifier, e=self.event_code, p=self.partition_code, z=self.zone_code)

    def __repr__(self):
        return "<Message {}, {}>".format(self.id, self.contact_id)


Session = sessionmaker(bind=engine)


Base.metadata.create_all(engine)


@contextmanager
def session_scope():
    """Provide a transactional scope around a series of operations."""
    session = Session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()
