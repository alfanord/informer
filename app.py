# -*- coding: utf-8 -*-
# noinspection SpellCheckingInspection
import config
import ari, uuid, sox
import asyncio, os, sched, time, datetime
import logging, colorlog

from multiprocessing import cpu_count
from concurrent.futures import ThreadPoolExecutor
from yandex_speech import TTS
from requests.exceptions import ConnectionError, HTTPError
from models import Object, Person, Partition, session_scope
from json.decoder import JSONDecodeError


# session = Session()


class DictPayload(object):
    def __init__(self, data):
        self.__dict__.update(data)


class PersonPriority:
    def __init__(self, sch):
        self.sch = sch

    @staticmethod
    def play_greeting(channel):
        channel.play(media='sound:ans-ivr-gratz-8')
        channel.play(media='sound:ans-ivr-stay-8')

    @staticmethod
    def set_prio(channel, person):
        logging.info('PersonIdentify from {}'.format(person.full_name))
        queue_prio, importance = person.priority
        callerid_name = '{} ({})'.format(person.full_name, importance)
        channel.setChannelVar(variable='CALLERID(name)', value=callerid_name)
        channel.setChannelVar(variable='QUEUE_PRIO', value=queue_prio)

    # @staticmethod
    # def recall(**kwargs):
    #     print(kwargs)

    # PERSON_IDENTIFY
    def run(self, channel, session):
        # self.sch.add_task(self.recall, kwargs={'person': 4321, 'object': 417, 'time': datetime.datetime.now()})
        channel.setChannelVar(variable='PERSON_IDENTIFY', value=0)

        self.play_greeting(channel)

        caller = channel.json.get('caller').get('number')
        person = session.query(Person).filter_by(phone_mobile=caller).first()

        if person is not None:
            self.set_prio(channel, person)
            channel.setChannelVar(variable='PERSON_IDENTIFY', value=1)

        channel.continueInDialplan()


class DTMF:
    def __init__(self, control, channel, person):
        self.control = control
        self.channel = channel
        self.person = person
        self.object = None
        self.dtmf = None
        self.person_input = ''

    def run(self):
        self.set_dtmf('start')
        self.ask_for_object()

    def ask_for_object(self):
        self.person_input = ''
        self.channel.play(media='sound:informer/ask-for-object')

    def set_dtmf(self, menu):
        if self.dtmf is not None:
            self.dtmf.close()

        if menu == 'start':
            self.dtmf = self.channel.on_event('ChannelDtmfReceived', self.on_dtmf_start)
        elif menu == 'repeat':
            self.dtmf = self.channel.on_event('ChannelDtmfReceived', self.on_dtmf_repeat)
        elif menu == 'object':
            self.dtmf = self.channel.on_event('ChannelDtmfReceived', self.on_dtmf_repeat)

    def on_dtmf_start(self, channel, event):
        digit = event['digit']
        if digit == '#':
            self.process_input()
        elif digit == '*':
            self.ask_for_object()
        else:
            self.person_input += str(digit)

    def on_dtmf_repeat(self, channel, event):
        digit = event['digit']
        if digit == '#':
            with session_scope() as s:
                s.add(self.object)
                s.refresh(self.object)
                self.report_state()
            self.set_dtmf('repeat')
        elif digit == '*':
            self.object = None
            self.ask_for_object()
            self.set_dtmf('start')

    def on_dtmf_object(self, channel, event):
        digit = event['digit']
        if digit == '#':
            with session_scope() as s:
                # s.add(self.object)
                # s.refresh(self.object)
                self.set_dtmf('repeat')
            self.report_state()
        elif digit == '*':
            self.object = None
            self.set_dtmf('start')
            self.ask_for_object()

    def report_object(self, person_object):
        self.object = person_object
        self.play_object()
        self.report_state()
        self.set_dtmf('repeat')

    def process_input(self):
        with session_scope() as session:
            session.add(self.person)
            if len(self.person_input) == 4:
                for person_object in self.person.objects:
                    if person_object.panel_code == self.person_input:
                        self.report_object(person_object)
            else:
                for person_object in self.person.objects:
                    if person_object.code == self.person_input:
                        self.report_object(person_object)

        if self.object is None:
            self.ask_for_object()

    def report_state(self):

        parts = sorted(self.object.partitions, key=lambda k: k.code)
        for part in parts:
            self.channel.play(media='sound:informer/part-{}'.format(part.code))
            self.channel.play(media='sound:informer/armed-{}'.format(int(part.state['armed'])))

        self.channel.play(media='sound:informer/repeat-report')

    def play_object(self):
        if self.object.media is None:
            self.object.media = self.control.get_yandex_tts('объект - {}'.format(self.object.title))
            # s.add(self.object)
        self.channel.play(media='sound:titles/{}'.format(self.object.media))


class ObjectControl:
    def __init__(self, sch):
        self.sch = sch

    def run(self, channel, session):

        channel.setChannelVar(variable='PERSON_IDENTIFY', value=0)

        caller = channel.json.get('caller').get('number')
        person = session.query(Person).filter_by(phone_mobile=caller).first()

        if person is not None:
            logging.info('ObjectControl from {}'.format(person.full_name))
            channel.setChannelVar(variable='PERSON_IDENTIFY', value=1)

            if person.media is None:
                person.media = self.get_yandex_tts('добрый день, {}. вас приветствует робот автоматического информирования.'.format(person.name))
                session.add(person)
            channel.play(media='sound:titles/{}'.format(person.media))

            dtmf = DTMF(self, channel, person)
            dtmf.run()
        else:
            channel.continueInDialplan()

    @staticmethod
    def unlink_media(file):
        if os.path.exists(file):
            os.remove(file)

    @staticmethod
    def get_yandex_tts(text):
        raw_media = uuid.uuid4()
        sox_media = uuid.uuid4()
        tts = TTS(speaker='oksana', audio_format='wav', key=config.YANDEX_API_KEY, lang='ru-RU', emotion="neutral", speed=1)
        tts.generate(text)
        # yandex_file = tts.save('/tmp/{}'.format(str(raw_media)))
        yandex_file = tts.save(os.path.join('/tmp', '{}'.format(str(raw_media))))
        tfm = sox.Transformer()
        tfm.convert(samplerate=8000)
        # tfm.build(yandex_file, '{}/{}.wav'.format(config.TTS_PATH, str(sox_media)))
        tfm.build(yandex_file, os.path.join(config.TTS_PATH, '{}.wav'.format(str(sox_media))))
        if os.path.exists(yandex_file):
            os.remove(yandex_file)
        return sox_media


class Scheduler:
    stopped = False

    def __init__(self):
        self.scheduler = sched.scheduler(time.time, time.sleep)

    async def waiter(self):
        while self.scheduler.empty() or not self.stopped:
            await asyncio.sleep(1.0)
        print('waiter stopped')

    async def main(self):
        logging.info('Scheduler started')
        while True or not self.stopped:
            await self.waiter()
            self.scheduler.run()
        print('main stopped')

    def stop(self):
        self.stopped = True

    def add_task(self, func, **kwargs):
        print(func, kwargs)
        self.scheduler.enter(10, 1, func, **kwargs)


class AriClient:
    def __init__(self, sch):
        self.client = None
        self.person_priority = PersonPriority(sch)
        self.object_control = ObjectControl(sch)

    def init_client(self):
        self.client = ari.connect('http://{}'.format(config.ARI_HOST), config.ARI_USERNAME, config.ARI_PASSWORD)
        self.client.on_channel_event('StasisStart', self.stasis_start_cb)
        self.client.on_channel_event('StasisEnd', self.stasis_end_cb)

    @staticmethod
    def stasis_end_cb(channel, ev):
        logging.debug('Stasis ends {}: {}'.format(channel, ev))

    def stasis_start_cb(self, channel, ev):
        stasis = ev.get('application')
        chan = channel.get('channel')

        if stasis == 'PersonPriority':
            with session_scope() as session:
                self.person_priority.run(chan, session)
        elif stasis == 'ObjectControl':
            with session_scope() as session:
                self.object_control.run(chan, session)

    def run(self):
        try:
            self.client.run(apps=config.ARI_APPS)
        except HTTPError as e:
            logging.error(e)
        except ConnectionError as e:
            logging.error(e)
        except ConnectionResetError as err:
            logging.error(err)
        except BrokenPipeError as err:
            logging.error(err)
        except JSONDecodeError as err:
            logging.error(err)

    def close(self):
        self.client.close()


if __name__ == "__main__":

    pool = ThreadPoolExecutor(max_workers=cpu_count())
    loop = asyncio.get_event_loop()
    scheduler = Scheduler()
    scheduler_task = loop.create_task(scheduler.main())
    client = AriClient(scheduler)
    client.init_client()
    loop.run_in_executor(pool, client.run)

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        logging.info('Exiting...')
    finally:
        scheduler.stop()
        scheduler_task.cancel()
        client.close()
        pool.shutdown(wait=True)
        loop.close()
